using System;
using System.Collections.Generic;
using Zenject;

public class UnitsSpawnedManager : IInitializable, IDisposable
{    
    [Inject] private UnitsSpawnedSettings unitsSpawnedSettings;
    [Inject] private DiContainer diContainer;
    [Inject] private FightManager fightManager;
    [Inject] private GamePlayViewModel gamePlayViewModel;

    private List<GameUnit> gameUnits = new List<GameUnit>();
    public List<GameUnit> GameUnits => gameUnits;

    public void Restart()
    {
        foreach (var unit in gameUnits)
        {
            unit.Restart();
        }
    }

    void IDisposable.Dispose()
    {
        gamePlayViewModel.RestartButtonTriggered -= Restart;
    }

    void IInitializable.Initialize()
    {
        gamePlayViewModel.RestartButtonTriggered += Restart;
        foreach (var spawnedSettings in unitsSpawnedSettings.SpawnedSettings)
        {
            GameUnit gameUnit = diContainer.InstantiatePrefabForComponent<GameUnit>(spawnedSettings.GameUnit);
            gameUnit.transform.position = spawnedSettings.SpawnPoint.transform.position;
            gameUnit.transform.rotation = spawnedSettings.SpawnPoint.transform.rotation;
            spawnedSettings.GameUnitController.Initialize(gameUnit);
            spawnedSettings.GameUnitController.GetComponent<IndicatorsView>().Initialize(gameUnit);
            spawnedSettings.GameUnitController.GetComponent<BuffView>().Initialize(gameUnit);
            fightManager.AddGameUnitInFigth(gameUnit);
            gameUnits.Add(gameUnit);
        }
    }
}
