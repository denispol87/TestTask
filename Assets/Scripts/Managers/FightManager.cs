using System;
using System.Collections.Generic;
using UniRx;
using Zenject;

public class FightManager : IInitializable, IDisposable
{
    private Queue<GameUnit> queueGameUnits = new Queue<GameUnit>();
    private Queue<GameUnit> newQeueGameUnits = new Queue<GameUnit>();
    private List<GameUnit> gameUnits = new List<GameUnit>();
    private GameUnit activeGameUnit;
    
    private IntReactiveProperty numberMoves = new IntReactiveProperty();
    public IReadOnlyReactiveProperty<int> NumberMoves => numberMoves;
        
    [Inject] private GamePlayViewModel gamePlayViewModel;

    public void AddGameUnitInFigth(GameUnit gameUnit)
    {
        gameUnits.Add(gameUnit);        
    }

    public void Attack(GameUnit gameUnit)
    {
        numberMoves.Value++;        
        foreach (var unit in queueGameUnits)
        {
            if (unit != gameUnit)
            {                
                unit.Hit(gameUnit);
            }            
        }
        foreach (var unit in newQeueGameUnits)
        {
            if (unit != gameUnit)
            {
                unit.Hit(gameUnit);
            }
        }

        foreach(var unit in gameUnits)
        {
            if(unit.Health.Value == 0)
            {
                gamePlayViewModel.InvokeRestartButtonTriggered();
                return;
            }
        }

        NextMove();
    }    
    
    private void NextMove()
    {
        if (queueGameUnits.Count == 0)
        {
            queueGameUnits = newQeueGameUnits;
            newQeueGameUnits = new Queue<GameUnit>();
            gamePlayViewModel.NumberRound.Value++;
        }
        activeGameUnit = queueGameUnits.Dequeue();
        if (activeGameUnit != null)
        {
            activeGameUnit.CanAttack.Value = true;
        }
        newQeueGameUnits.Enqueue(activeGameUnit);
    }

    private void Restart()
    {
        gamePlayViewModel.NumberRound.Value = 0;
        queueGameUnits.Clear();
        newQeueGameUnits.Clear();
        foreach (var unit in gameUnits)
        {
            queueGameUnits.Enqueue(unit);
        }
        NextMove();
    }

    void IDisposable.Dispose()
    {
        gamePlayViewModel.RestartButtonTriggered -= Restart;
    }

    void IInitializable.Initialize()
    {
        Restart();
        gamePlayViewModel.RestartButtonTriggered += Restart;
    }
}