﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using Zenject;

public class BuffManager : IInitializable, IDisposable
{ 
    private List<Buff> buffs = new List<Buff>();
    void IDisposable.Dispose()
    {

    }

    void IInitializable.Initialize()
    {
        buffs.Add(new Buff(BuffType.DoubleDamage, damageMultiplier: 2));
        buffs.Add(new Buff(BuffType.ArmorSelf, additionalArmor: 50));
        buffs.Add(new Buff(BuffType.ArmorDestruction, additionalArmorForTheEnemy: -10));
        buffs.Add(new Buff(BuffType.VampirismSelf, additionalArmor: -25, additionalVampirism: 50));
        buffs.Add(new Buff(BuffType.VampirismDecrease, additionalVampirismForTheEnemy: -25));
    }

    public Buff GetRandomBuff(ReactiveCollection<Buff> activeBuffs)
    {   
        List<Buff> notActiveBuffs = new List<Buff>();
        foreach (var buff in buffs)
        {
            if (activeBuffs.FirstOrDefault(v => v.BuffType == buff.BuffType) == null)
                notActiveBuffs.Add(buff);
        }

        if (notActiveBuffs.Count > 0)
            return notActiveBuffs[new Random().Next(notActiveBuffs.Count)].Clone();
        else
            return null;
    }

    public string GetDescription(BuffType buffType)
    {
        switch (buffType)
        {
            case BuffType.DoubleDamage:
                return "Double damage";
            case BuffType.ArmorSelf:
                return "Armor self";
            case BuffType.VampirismSelf:
                return "Vampirism self";
            case BuffType.VampirismDecrease:
                return "Vampirism decrease";
            case BuffType.ArmorDestruction:
                return "Armor destruction";
            default:
                return "???";
        }
    }
}

public enum BuffType
{
    DoubleDamage = 1,
    ArmorSelf = 2,
    ArmorDestruction = 3,
    VampirismSelf = 4,
    VampirismDecrease = 5,
}
