using System.Linq;
using UnityEngine;

public class TurningCanvasToCamera : MonoBehaviour
{
    private Camera renderingCamera;
    [SerializeField] private Vector3 vector3;

    private void OnEnable()
    {
        renderingCamera = Camera.allCameras.FirstOrDefault(i => (i.cullingMask & gameObject.layer) == gameObject.layer);
    }

    private void LateUpdate()
    {
        transform.rotation = renderingCamera.transform.rotation;
        //transform.LookAt(renderingCamera.transform, vector3);
    }
}
