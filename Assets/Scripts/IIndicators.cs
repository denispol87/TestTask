﻿using UniRx;

public interface IIndicators
{
    public IReadOnlyReactiveProperty<int> Health { get; }
    public IReadOnlyReactiveProperty<int> Armor { get; }
    public IReadOnlyReactiveProperty<int> Vampirism { get; }
    public IReadOnlyReactiveProperty<int> Damage { get; }    
}