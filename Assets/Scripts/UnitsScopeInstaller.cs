using UnityEngine;
using Zenject;

public class UnitsScopeInstaller : MonoInstaller
{
    [SerializeField] private UnitsSpawnedSettings unitsSpawnedSettings;    
    [SerializeField] private BuffUILine buffUILine;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<UnitsSpawnedManager>()
           .FromNew()
           .AsSingle()
           .NonLazy();
        
        Container.BindInterfacesAndSelfTo<BuffManager>()
            .FromNew()
            .AsSingle()
            .NonLazy();       

        Container.BindInterfacesAndSelfTo<FightManager>()
            .FromNew()
            .AsSingle()
            .NonLazy();
        
        Container.Bind<GamePlayViewModel>()
            .FromNew()
            .AsSingle()
            .NonLazy();
        
        Container.BindExecutionOrder<UnitsSpawnedManager>(-1000);
        Container.BindExecutionOrder<FightManager>(-999);

        Container.BindInstance(unitsSpawnedSettings);        
        Container.BindInstance(buffUILine);
    }
}
