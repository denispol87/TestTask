﻿using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using Zenject;
using UniRx;

public class BuffUILine : MonoBehaviour
{
    [Inject] private BuffManager buffManager;

    [SerializeField] private TextMeshProUGUI infoText;
    private Buff buff;
    public void Initialize(Buff buff)
    {
        this.buff = buff;
        buff.DurationsInRounds.Subscribe(UpdateValue).AddTo(this);
    }

    private void UpdateValue(int durationsInRounds)
    {        
        if (durationsInRounds == 0)
            return;

        infoText.SetText($"{buffManager.GetDescription(buff.BuffType)} ({durationsInRounds})");
    }
}