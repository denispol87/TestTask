﻿using UniRx;

public interface IBuff
{
    public IReadOnlyReactiveCollection<Buff> ActiveBuffs { get; }
}