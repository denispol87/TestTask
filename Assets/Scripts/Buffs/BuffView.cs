﻿using UnityEngine;
using UniRx;
using System.Collections.Generic;
using Zenject;

public class BuffView : MonoBehaviour
{
    [SerializeField] private Transform content;

    [Inject] private DiContainer diContainer;
    [Inject] private BuffUILine buffUILine;
    [Inject] private GamePlayViewModel gamePlayViewModel;

    private List<BuffUILine> buffUILines = new List<BuffUILine>();

    public void Initialize(IBuff iBuff)
    {
        iBuff.ActiveBuffs.ObserveAdd().Subscribe(v => { AddLine(v.Value); }).AddTo(this);
        gamePlayViewModel.RestartButtonTriggered += Restart;
    }

    private void OnDestroy()
    {
        gamePlayViewModel.RestartButtonTriggered -= Restart;
    }

    private void Restart()
    {
        for (int i = buffUILines.Count-1; i>=0; i--)
        {
            DestroyBuffLine(buffUILines[i]);
        }
    }

    private void AddLine(Buff buff)
    {
        BuffUILine buffLine = diContainer.InstantiatePrefabForComponent<BuffUILine>(buffUILine);
        buffLine.Initialize(buff);
        buffLine.transform.SetParent(content.transform);
        buff.DurationsInRounds.Subscribe(v => {
            if (v == 0)            
                DestroyBuffLine(buffLine);             
        }).AddTo(buffLine);

        buffUILines.Add(buffLine);
    }

    private void DestroyBuffLine(BuffUILine buffLine)
    {
        buffUILines.Remove(buffLine);
        DestroyImmediate(buffLine.gameObject);
    }
}