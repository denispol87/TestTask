﻿using UniRx;
using UnityEngine;

public class Buff
{
    public readonly BuffType BuffType;
    public readonly int DamageMultiplier;
    public readonly int AdditionalArmor;
    public readonly int AdditionalArmorForTheEnemy;
    public readonly int AdditionalVampirism;
    public readonly int AdditionalVampirismForTheEnemy;

    private IntReactiveProperty durationsInRounds = new IntReactiveProperty();
    public IReadOnlyReactiveProperty<int> DurationsInRounds => durationsInRounds;

    public Buff(BuffType buffType, int damageMultiplier = 1, int additionalArmor = 0, int additionalArmorForTheEnemy = 0, int additionalVampirism = 0, int additionalVampirismForTheEnemy = 0)
    {
        BuffType = buffType;
        DamageMultiplier = damageMultiplier;
        AdditionalArmor = additionalArmor;
        AdditionalArmorForTheEnemy = additionalArmorForTheEnemy;
        AdditionalVampirism = additionalVampirism;
        AdditionalVampirismForTheEnemy = additionalVampirismForTheEnemy;
    }   

    public void SetDuration(int durations)
    {
        durationsInRounds.Value = durations;
    }

    public void ReducingDuration()
    {
        durationsInRounds.Value--;
    }

    public Buff Clone()
    {
        return new Buff(BuffType, DamageMultiplier, AdditionalArmor, AdditionalArmorForTheEnemy, AdditionalVampirism, AdditionalVampirismForTheEnemy);
    }
}