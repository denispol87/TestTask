﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitsSpawnedSettings
{
    [SerializeField] private List<UnitSpawnedSettings> spawnedSettings;    
    public List<UnitSpawnedSettings> SpawnedSettings => spawnedSettings;
}

[Serializable]
public class UnitSpawnedSettings
{
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private GameUnit gameUnit;
    [SerializeField] private GameUnitController gameUnitController;

    public Transform SpawnPoint => spawnPoint;
    public GameUnit GameUnit => gameUnit;
    public GameUnitController GameUnitController => gameUnitController;
}
