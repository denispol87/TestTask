﻿using UniRx;
using UnityEngine;
using Zenject;

public class GameUnit : MonoBehaviour, IIndicators, ICharacterManagement, IBuff
{
    [SerializeField] private ColorizationMaterial[] colorizationMaterials;

    private int baseDamage = 15;
    private int maxHealth = 100;
    private int maxArmor = 100;
    private int maxVampirism = 100;    
    private int maxBuffCount = 2;
    private int maxBuffDurationRound = 5;

    [Inject] private BuffManager buffManager;
    [Inject] private FightManager fightManager;
    [Inject] private GamePlayViewModel gamePlayViewModel;

    private IntReactiveProperty health = new IntReactiveProperty();
    private IntReactiveProperty armor = new IntReactiveProperty();
    private IntReactiveProperty vampirism = new IntReactiveProperty();
    private IntReactiveProperty damage = new IntReactiveProperty();

    public IReadOnlyReactiveProperty<int> Health => health;
    public IReadOnlyReactiveProperty<int> Armor => armor;
    public IReadOnlyReactiveProperty<int> Vampirism => vampirism;
    public IReadOnlyReactiveProperty<int> Damage => damage;

    public BoolReactiveProperty CanAttack { get; } = new BoolReactiveProperty();
    
    private BoolReactiveProperty canApplyRandomBuff = new BoolReactiveProperty();
    public IReactiveProperty<bool> CanApplyRandomBuff => canApplyRandomBuff;

    private ReactiveCollection<Buff> activeBuffs = new ReactiveCollection<Buff>();
    public IReadOnlyReactiveCollection<Buff> ActiveBuffs => activeBuffs;

    private void Start()
    {
        SetHealth(maxHealth);
        SetArmor(0);
        SetVampirism(0);
        damage.Value = baseDamage;
        
        CanAttack.Subscribe(value => {
            if (value)
                CanApplyRandomBuff.Value = ActiveBuffs.Count < maxBuffCount;
            else            
                CanApplyRandomBuff.Value = false;
        }).AddTo(this);

        ActiveBuffs.ObserveCountChanged().Subscribe(v => {      
            CanApplyRandomBuff.Value = v < maxBuffCount && CanAttack.Value;
        }).AddTo(this);
    }

    public void Restart()
    {
        activeBuffs.Clear();
        CanAttack.Value = false;
        canApplyRandomBuff.Value = false;        

        health.Value = maxHealth;
        armor.Value = 0;
        vampirism.Value = 0;
        damage.Value = baseDamage;
    }

    public void SetHealth(int value)
    {
        health.Value = Mathf.Clamp(health.Value + value, 0, maxHealth);
    }

    public void SetArmor(int value)
    {
        armor.Value = Mathf.Clamp(armor.Value + value, 0, maxArmor);
    }

    public void SetVampirism(int value)
    {
        vampirism.Value = Mathf.Clamp(vampirism.Value + value, 0, maxVampirism);
    }    

    public void ApplyRandomBuff()
    {
        Buff buff = buffManager.GetRandomBuff(activeBuffs);
        if (buff != null)
        {            
            buff.SetDuration(Random.Range(1, maxBuffDurationRound));
            UseBuff(buff);
            activeBuffs.Add(buff);
        }
    }

    private void UseBuff(Buff buff)
    {
        damage.Value *= buff.DamageMultiplier;
        SetArmor(buff.AdditionalArmor);
        SetVampirism(buff.AdditionalVampirism);
    }

    private void CancelBuff(Buff buff)
    {
        damage.Value /= buff.DamageMultiplier;
        SetArmor(-buff.AdditionalArmor);
        SetVampirism(-buff.AdditionalVampirism);        

        Debug.Log("CancelBuff " + name);
        activeBuffs.Remove(buff);
    }

    public void Attack()
    {        
        fightManager.Attack(this);
        CanAttack.Value = false;
        ReducingDurationBuffs();
    }

    private void ReducingDurationBuffs()
    {
        for (int i = ActiveBuffs.Count-1; i >=0; i--)
        {
            var buff = ActiveBuffs[i];
            if (buff.DurationsInRounds.Value == 1)
                CancelBuff(buff);
            buff.ReducingDuration();
        }
    }

    public void Hit(GameUnit gameUnit)
    {
        foreach (var buff in gameUnit.ActiveBuffs)
        {
            SetArmor(buff.AdditionalArmorForTheEnemy);
            SetVampirism(buff.AdditionalVampirismForTheEnemy);
        }
        int hitDamag = Mathf.FloorToInt((float)gameUnit.Damage.Value * ((100f - (float)armor.Value) / 100f));
        gameUnit.SetHealth(Mathf.FloorToInt((float)hitDamag * (float)gameUnit.Vampirism.Value / 100f));

        if (hitDamag > 0)
            HitEffect();

        SetHealth(-hitDamag);
    }

    public void HitEffect()
    {
        foreach(var cMaterials in colorizationMaterials)
        {
            cMaterials.Flash(Color.red, 1);
        }
    }
}