using System;
using UniRx;

public class GamePlayViewModel 
{
    public IntReactiveProperty NumberRound = new IntReactiveProperty();    
    public event Action RestartButtonTriggered = delegate { };

    public void InvokeRestartButtonTriggered()
    {
        RestartButtonTriggered.Invoke();
    }
}
