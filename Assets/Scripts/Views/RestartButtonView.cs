using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Button))]
public class RestartButtonView : MonoBehaviour
{
    [SerializeField] private Button button;

    [Inject] GamePlayViewModel gamePlayViewModel;

    void Start()
    {
        button.onClick.AddListener(gamePlayViewModel.InvokeRestartButtonTriggered);
    }

#if UNITY_EDITOR
    private void Reset()
    {
        button = GetComponent<Button>();
    }
#endif
}
