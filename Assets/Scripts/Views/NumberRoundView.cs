using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using Zenject;
using UniRx;

[RequireComponent(typeof(TextMeshProUGUI))]
public class NumberRoundView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [Inject] private GamePlayViewModel gamePlayViewModel;

    private void Start()
    {
        gamePlayViewModel.NumberRound.Subscribe(v => {
            ChangeValue(v);
        }).AddTo(this);
        gamePlayViewModel.RestartButtonTriggered += Restart;
    }

    private void OnDestroy()
    {
        gamePlayViewModel.RestartButtonTriggered -= Restart;
    }

    private void Restart()
    {
        ChangeValue(0);
    }

    private void ChangeValue(int value)
    {
        text.SetText($"Round: {value}");
    }

#if UNITY_EDITOR
    private void Reset()
    {
        text = GetComponent<TextMeshProUGUI>();        
    }
#endif
}
