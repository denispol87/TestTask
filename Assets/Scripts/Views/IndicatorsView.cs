using Cysharp.Threading.Tasks;
using TMPro;
using UniRx;
using UnityEngine;

public class IndicatorsView : MonoBehaviour
{
    [SerializeField] private GameUnit gameUnit;
    [SerializeField] private TextMeshProUGUI health, armor, vampirism, damage;

    private void Start()
    {
        if (gameUnit != null)
            Initialize(gameUnit);
    }

    public void Initialize(IIndicators iindicators)
    {
        iindicators.Health.Subscribe(v => health.SetText(v.ToString())).AddTo(this);
        iindicators.Armor.Subscribe(v => armor.SetText(v.ToString())).AddTo(this);
        iindicators.Vampirism.Subscribe(v => vampirism.SetText(v.ToString())).AddTo(this);
        iindicators.Damage.Subscribe(v => damage.SetText(v.ToString())).AddTo(this);
    }
}
