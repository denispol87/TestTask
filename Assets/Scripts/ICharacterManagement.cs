﻿using UniRx;

public interface ICharacterManagement
{
    public BoolReactiveProperty CanAttack { get; }
    public IReactiveProperty<bool> CanApplyRandomBuff { get; }
    public void Attack();
    public void ApplyRandomBuff();
}