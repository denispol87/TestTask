using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class GameUnitController : MonoBehaviour
{   
    [SerializeField] private Button attackButton;
    [SerializeField] private Button randomBuffButton;    

    public void Initialize(ICharacterManagement iCharacterManagement)
    {
        attackButton.onClick.RemoveAllListeners();
        randomBuffButton.onClick.RemoveAllListeners();
        attackButton.onClick.AddListener(iCharacterManagement.Attack);
        randomBuffButton.onClick.AddListener(iCharacterManagement.ApplyRandomBuff);

        iCharacterManagement.CanAttack.Subscribe(v => {
            attackButton.interactable = v;
        }).AddTo(this);

        iCharacterManagement.CanApplyRandomBuff.Subscribe(v => {
            randomBuffButton.interactable = v;
        }).AddTo(this);        
    }
}
