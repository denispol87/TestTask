using Cysharp.Threading.Tasks;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(SkinnedMeshRenderer))]
public class ColorizationMaterial : MonoBehaviour
{
    private SkinnedMeshRenderer skinnedMeshRenderer;
    private Color startColor;

    private void Start()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        startColor = skinnedMeshRenderer.material.color;       
    }

    public void Flash(Color to, float duration)
    {
        Flash(to, startColor, duration).Forget();
    }

    public async UniTaskVoid Flash(Color from, Color to, float duration)
    {
        Material targetMaterial = skinnedMeshRenderer.materials.Last();       
        for (float i = 0; i < duration; i += Time.deltaTime)
        {
            targetMaterial.color = Color.Lerp(from, to, i / duration);
            await UniTask.Yield();
        }
        targetMaterial.color = Color.Lerp(from, to, 1);
    }
}
